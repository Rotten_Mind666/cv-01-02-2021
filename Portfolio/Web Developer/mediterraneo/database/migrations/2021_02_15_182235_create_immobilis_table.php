<?php

use App\Models\Immobili;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImmobilisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('immobilis', function (Blueprint $table) {
            $table->id();
            $table->string('nome'); //a
            $table->text('descrizione'); //b
            $table->string('via'); //c
            $table->integer('civico'); //d
            $table->integer('cap'); //e
            $table->string('provincia'); //f
            $table->string('citta'); //g
            $table->boolean('castellabate')->nullable()->default(false); //h
            $table->boolean('saviano')->nullable()->default(false); //i
            $table->boolean('sommavesuviana')->nullable()->default(false); //l
            $table->integer('stanze'); //m
            $table->integer('mq'); //n
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('immobilis');
    }
}
