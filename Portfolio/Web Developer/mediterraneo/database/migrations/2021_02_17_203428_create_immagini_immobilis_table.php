<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImmaginiImmobilisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('immagini_immobilis', function (Blueprint $table) {
            $table->id();
            $table->string('immagini')->nullable();
            $table->foreignId('immobili_id')->nullable()->constrained()->onDelete('cascade');
            $table->boolean('castellabate')->nullable()->default(false); //h
            $table->boolean('saviano')->nullable()->default(false); //i
            $table->boolean('sommavesuviana')->nullable()->default(false); //l
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('immagini_immobilis', function (Blueprint $table) {
            $table->dropForeign(['immobili']);
        });
        Schema::dropIfExists('immagini_immobilis');
    }
}
