@extends('layouts.app')

@section('body')
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
<header>
  <div class="overlay"></div>
  <video id="video1" class="d-none" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source  src="video/video1.mp4" type="video/mp4">
  </video>
  <video id="video2" class="d-none" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source  src="video/Sequenza01.mp4" type="video/mp4">
  </video>
  <video id="video3" class="d-none" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source  src="video/video2.mp4" type="video/mp4">
  </video>
  <video id="video4" class="" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source  src="video/video3.mp4" type="video/mp4">
  </video>
  <div class="container h-100">
    <div class="d-flex h-100 text-center align-items-center">
      <div class="w-100 text-white">        
        <div class="container-fluid my-5 py-5">
          <h1 class="text-dark">Scegli la destinazione piu ideale per te!</h1>
          <div class="row justify-content-center">
            
              
            <a href="{{route('case.castellabate')}}" onmouseover="castellabate()" onmouseout="nulla()" style="border: solid 2px grey;height: 100px;background-image: url('image/CASTELLABATE.png');background-size: 100%; background-position: center; background-repeat: no-repeat" class="col-12 btn col-md-3 m-3" id="castellabate">
              <span class="badge badge-dark rounded-circle">{{count($ca)}}</span>  
            </a>
              
            <a href="{{route('case.saviano')}}" onmouseover="saviano()" onmouseout="nulla()" style="border: solid 2px grey;height: 100px;background-image: url('image/SAVIANO.png');background-size: 100%; background-position: center; background-repeat: no-repeat" class="col-12 btn col-md-3 m-3" id="saviano">    
              <span class="badge badge-dark rounded-circle">{{count($sa)}}</span>  
            </a>
            
            <a href="{{route('case.sommavesuviana')}}" onmouseover="sommavesuviana()" onmouseout="nulla()" style="border: solid 2px grey;height: 100px;background-image: url('image/SOMMAVESUVIANA.png');background-size: 85%; background-position: center; background-repeat: no-repeat" class="col-12 btn col-md-3 m-3" id="sommavesuviana">
              <span class="badge badge-dark rounded-circle">{{count($so)}}</span>  
            </a>
 
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<script>
  let a = document.getElementById('video1').classList; 
  let b = document.getElementById('video2').classList; 
  let c = document.getElementById('video3').classList;
  let d = document.getElementById('video4').classList;
  
  function nulla() {
    a.add('d-none');
    b.add('d-none');
    c.add('d-none');
    d.remove('d-none');
  }
  function castellabate() {
    b.remove('d-none');
    d.add('d-none');
  }
  function saviano() {
    a.remove('d-none');
    d.add('d-none');
  }
  function sommavesuviana() {
    c.remove('d-none');
    d.add('d-none');
  }
</script>
@endsection