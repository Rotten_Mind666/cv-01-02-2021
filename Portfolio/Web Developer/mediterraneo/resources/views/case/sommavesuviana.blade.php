@extends('layouts.app')

@section('body')
<div class="container-fluid">
    <div class="text-center my-5"><img class="w-50" src="image/SOMMAVESUVIANA.png" alt=""></div>
  <div class="row justify-content-center">

    @foreach ($immobili as $immobile)
      <div class="card mx-2" style="max-width: 22rem;">
        
        <img {{$i = $immobile->id }} class="card-img-top w-100" src="{{ Storage::url($immagini[$i]->immagini) }}" alt="Card image cap">

        <div class="card-body">
          {{-- <h2 class="card-title">{{$immobile->nome}} lore</h2> --}}
          <h4 class="card-title text-capitalize">{{$immobile->via}} {{$immobile->civico}}, {{$immobile->citta}} </h4>
          <p>Stanze: <span class="font-weight-bold">{{$immobile->stanze}}</span></p>
          <p><span class="font-weight-bold">{{$immobile->mq}}</span> m²</p>
          <div>Servizi: 
            <div>
              <i class="fas fa-fire-alt fa-2x mx-1"></i>
              <i class="fas fa-tint fa-2x mx-1"></i> 
              <i class="fas fa-bolt fa-2x mx-1"></i>
              <i class="fas fa-parking fa-2x mx-1"></i>
              [...]
            </div>
          </div>
          <a href="{{route('case.show', $immobile)}}" class="btn btn-primary float-right">Vedi dettagli</a>
        </div>
      </div>

    @endforeach

  </div>
</div>
@endsection
