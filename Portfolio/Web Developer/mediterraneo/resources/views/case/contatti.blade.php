@extends('layouts.app')

@section('body')
<div class="container-fluid">
    <div class="row justify-content-center">
    <div class="col-8">
        <form action="{{route('case.send')}}" method="post">
            @csrf
            <div class="form-group my-3">
                <label>Richiedi info per</label>
                <input class="form-control" name="casa" value="{{$immobili->nome}}" disabled>
            </div>
            <div class="form-group">
                <label>Nome</label>
                <input class="form-control" type="text" {{old('nomex')}} name="nome">
            </div>
            <div class="form-group">
                <label>Recapito telefonico</label>
                <input class="form-control" type="tel" value="{{old('numero')}}" name="numero">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="email" value="{{old('email')}}" name="email">
            </div>
            <div class="form-group">
                <label>Messaggio</label>
                <textarea class="form-control" name="messaggio" cols="30" rows="10">{{old('messaggio')}}</textarea>
            </div>
            <button type="submit" class="btn btn-dark float-right mb-3">Invia messaggio</button>
        </form>
    </div>
  </div>
</div>
<img src="https://cdn.pixabay.com/photo/2016/02/07/21/03/computer-1185626_960_720.jpg" class="h-25 w-100" alt="">
@endsection