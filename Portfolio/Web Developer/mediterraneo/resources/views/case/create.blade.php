@extends('layouts.app')
@section('body')

    <form enctype="multipart/form-data" action="{{route('store')}}" method="POST">
        @csrf

        <label class="text-capitalize" for="nome">nome</label>
        <input name="nome" type="text">

        <label class="text-capitalize" for="descrizione">descrizione</label>
        <textarea name="descrizione" cols="30" rows="10"></textarea>

        <label class="text-capitalize" for="via">via</label>
        <input name="via" type="text">

        <label class="text-capitalize" for="civico">civico</label>
        <input name="civico" type="number">

        <label class="text-capitalize" for="cap">cap</label>
        <input name="cap" type="number">

        <label class="text-capitalize" for="provincia">provincia</label>
        <input name="provincia" type="text">

        <label class="text-capitalize" for="citta">citta</label>
        <input name="citta" type="text">

        <div class="form-group">
            <label class="form-check-label" for="castellabate">Castellabate</label>
            <input class="form-control" value="1" type="checkbox" name="castellabate" id="flexRadioDefault1">
            
            <label class="form-check-label" for="saviano">Saviano</label>
            <input class="form-control" value="1" type="checkbox" name="saviano" id="flexRadioDefault2">
            
            <label class="form-check-label" for="sommavesuviana">Somma Vesuviana</label>
            <input class="form-control" value="1" type="checkbox" name="sommavesuviana" id="flexRadioDefault3">
        </div>

        <label class="text-capitalize" for="stanze">stanze</label>
        <input name="stanze" type="number">

        <label class="text-capitalize" for="mq">mq</label>
        <input name="mq" type="number">

        <label class="text-capitalize" for="immagini">immagini</label>
        <input name="immagini[]" type="file" multiple>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Block level button</button>
    </form>

@endsection