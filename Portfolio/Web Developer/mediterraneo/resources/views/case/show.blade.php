@extends('layouts.app')

@section('body')
<div class="container">
  <div class="row">
    

      <div id="carouselExampleControls" class="carousel slide my-3" data-ride="carousel">
        <div class="carousel-inner">
          @foreach ($immagini as $item)
          <div class="carousel-item {{$loop->iteration == 1 ? 'active' : ''}}">
            {{-- <img class="w-100" src="{{ Storage::url("$item->immagini") }}" alt=""> --}}
            <img class="w-100" src="https://picsum.photos/1000/500" alt="">
          </div>
          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon bg-dark rounded-circle" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon bg-dark rounded-circle" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    

    <div class="col-12">
      <div class="row p mx-1">
        <h1>{{$immobili->nome}}</h1>
        <a href="{{route('case.contatti', $immobili)}}" class="btn bg-dark text-white h-100">Prenota Ora!</a>
      </div>
        <div class="row c p-3">
            <i class="fa fa-map-marker p-1" aria-hidden="true"></i>
            <p>{{$immobili->cap}}</p>
            <p class="px-1">Via {{$immobili->via}}</p>
            <p>{{$immobili->civico}}, </p>
            <p class="pl-1">({{$immobili->provincia}}) - </p>
            @if ($immobili->castellabate == 1)
            <a href="https://www.google.it/maps/place/{{$immobili->via}}+Castellabate" class="font-weight-bold px-1">Castellabate</a>
            @endif
            @if ($immobili->saviano == 1)
            <a href="https://www.google.it/maps/place/{{$immobili->via}}+Saviano" class="font-weight-bold px-1">Saviano</a>
            @endif
            @if ($immobili->sommavesuviana == 1)
            <a href="https://www.google.it/maps/place/{{$immobili->via}}+SommaVesuviana" class="font-weight-bold px-1">Somma Vesuviana</a>
            @endif
        </div>
        
        <div class="row justify-content-around">
          <div class="text-center">
            <h2>{{$immobili->stanze}}</h2>
            <p class="text-muted">Stanze</p>
          </div>
          <div class="text-center">
            <h2>{{$immobili->mq}}</h2>
            <p>Superficie</p>
          </div>
          <div class="text-center mx-1">
            <i title="Gas" class="fas fa-fire-alt fa-2x mx-1"></i>
            <i title="Acqua" class="fas fa-tint fa-2x mx-1"></i> 
            <i title="Luce" class="fas fa-bolt fa-2x mx-1"></i>
            <i title="Parcheggio" class="fas fa-parking fa-2x mx-1"></i>
            <i title="Assistenza H24" class="fas fa-history fa-2x mx-1"></i>
            <i title="TV" class="fas fa-tv fa-2x mx-1"></i>

            <p>Servizi</p>
          </div>
        </div>
      <h2 class="text-center">{{$immobili->descrizione}}</h2>

    </div>
      
  </div>
</div>
@endsection

