<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImmobiliController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ImmobiliController::class, 'index'])->name('benvenuti');
Route::get('/castellabate', [ImmobiliController::class, 'indexCastellabate'])->name('case.castellabate');
Route::get('/saviano', [ImmobiliController::class, 'indexSaviano'])->name('case.saviano');
Route::get('/sommavesuviana', [ImmobiliController::class, 'indexSommavesuviana'])->name('case.sommavesuviana');
Route::get('/mostra/{immobili}', [ImmobiliController::class, 'show'])->name('case.show');
Route::get('/crea', [ImmobiliController::class, 'create'])->name('case.create');
Route::get('/contatti/{immobili}', [ImmobiliController::class, 'contact'])->name('case.contatti');
Route::post('/send', [ImmobiliController::class, 'send'])->name('case.send');
Route::post('/salvataggio', [ImmobiliController::class, 'store'])->name('store');

