<?php

namespace App\Http\Controllers;

use App\Models\Immobili;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Models\ImmaginiImmobili;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ImmobiliController extends Controller
{
    public function index()
    {
        $ca = Immobili::where('castellabate', true)->get();
        $sa = Immobili::where('saviano', true)->get();
        $so = Immobili::where('sommavesuviana', true)->get();
        return view('benvenuti', compact('ca','sa','so'));
    }
    public function indexCastellabate()
    {
        $immobili = Immobili::where('castellabate', true)->get();
        $immagini = ImmaginiImmobili::where('castellabate', true)->get();
        return view('case.castellabate', compact('immobili','immagini'));
    }
    public function indexSaviano()
    {
        $immobili = Immobili::where('saviano', true)->get();
        $immagini = ImmaginiImmobili::where('saviano', true)->get();
        return view('case.saviano', compact('immobili','immagini'));
    }
    public function indexSommavesuviana()
    {
        $immobili = Immobili::where('sommavesuviana', true)->get();
        $immagini = ImmaginiImmobili::where('sommavesuviana', true)->get();
        return view('case.sommavesuviana', compact('immobili','immagini'));
    }

    public function create(Request $request)
    {
        return view('case.create');
    }

    public function store(Request $request)
    {
        $immobili = Immobili::create ([
            'nome' => $request->input('nome'),
            'descrizione' => $request->input('descrizione'),
            'via' => $request->input('via'),
            'civico' => $request->input('civico'),
            'cap' => $request->input('cap'),
            'provincia' => $request->input('provincia'),
            'citta' => $request->input('citta'),
            'castellabate' => $request->input('castellabate'),
            'saviano' => $request->input('saviano'),
            'sommavesuviana' => $request->input('sommavesuviana'),
            'stanze' => $request->input('stanze'),
            'mq' => $request->input('mq'),
            ]);

            foreach ($request->file('immagini') as $item) {
                $i = new ImmaginiImmobili();
                $path = $item->store("public/$immobili->id");

                $i->immagini = $path;
                $i->immobili_id = $immobili->id;
                $i->castellabate = $request->input('castellabate');
                $i->saviano = $request->input('saviano');
                $i->sommavesuviana = $request->input('sommavesuviana');
                $i->save();
            }
            return redirect(route('benvenuti'));
    }

    public function show(Immobili $immobili)
    {   $e = $immobili->id;
        $immagini = ImmaginiImmobili::where('immobili_id',$e)->get();
        return view('case.show', compact('immobili', 'immagini'));
    }

    public function contact(Request $request, Immobili $immobili)
    {
        return view('case.contatti', compact('immobili'));
    }
    public function send(Request $request)
    {
        $casa = $request->input('casa');
        $nome = $request->input('nome');
        $numero = $request->input('numero');
        $email = $request->input('email');
        $messaggio = $request->input('messaggio');

        $tutto = compact('casa','nome','numero','messaggio','email');
    
        Mail::to('santolotoscano@gmail.com')->send(new ContactMail($tutto));
        return redirect(route('benvenuti'))->with('message', 'Grazie per aver chiesto info, vi risponderemo a breve!');
    }
}
