<?php

namespace App\Models;

use App\Models\ImmaginiImmobili;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Immobili extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $casts = [
        'castellabate' => 'boolean',
        'saviano' => 'boolean',
        'sommavesuviana' => 'boolean',
    ];
    
    public function immagini()
    {
        return $this->hasMany(ImmaginiImmobili::class);
    }
}
