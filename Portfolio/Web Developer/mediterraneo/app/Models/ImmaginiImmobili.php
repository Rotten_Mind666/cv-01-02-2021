<?php

namespace App\Models;

use App\Models\Immobili;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ImmaginiImmobili extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function immobili()
    {
        return $this->BelongsTo(Immobili::class);
    }
}
