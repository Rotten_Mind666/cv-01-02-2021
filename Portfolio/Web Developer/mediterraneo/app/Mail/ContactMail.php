<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $tutto;

    public function __construct($tutto)
    {
        $this->tutto = $tutto;
    }

    public function build()
    {
        return $this->from('cannavacciulo@gmail.nonnt')->view('case.mail');
    }
}
