<?php

use App\Models\Utilizzo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilizzosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utilizzos', function (Blueprint $table) {
            $table->id();
            $table->string('utilita')->nullable();
            $table->timestamps();
        });

        $utilizzi = [
            'ABITATIVO',
            'ATTIVITA COMMERCIALE',
            'AUTORIMESSA',
            'CANTINA',
            'DEPOSITO',
            'LABORATORIO',
            'MAGAZZINO',
            'NEGOZIO',
            'POSTO AUTO',
            'SOFFITTA',
            'TURISTICO',
            'UFFICIO',
            'RUDERE',
            'MASSERIA',
        ];
        foreach ($utilizzi as $utilizzo) {
            $u = new Utilizzo();
            $u->utilita = $utilizzo;
            $u->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilizzos');
    }
}
