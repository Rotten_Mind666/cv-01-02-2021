<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenditasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renditas', function (Blueprint $table) {
            $table->id();
            $table->float('renditacatastale')->nullable();
            $table->boolean('accatastamento')->default(false)->nullable();
            $table->float('pigione')->nullable();
            $table->foreignId('proprieta_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renditas', function (Blueprint $table) {
            $table->dropForeign(['proprieta_id']);
        });
        Schema::dropIfExists('renditas');
    }
}
