<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePiantinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piantinas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->float('mq')->nullable();
            $table->integer('anni')->nullable();
            $table->string('vani')->nullable();
            $table->string('pianta')->nullable();
            $table->foreignId('proprieta_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('piantinas', function (Blueprint $table) {
            $table->dropForeign(['proprieta_id']);
        });
        Schema::dropIfExists('piantinas');
    }
}
