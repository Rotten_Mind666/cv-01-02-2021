<?php

use App\Models\Categoria;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->text('descrizione')->nullable();
            $table->timestamps();
        });

        $descrizione = [
            'A/1: abitazioni di tipo signorile. Gli immobili che appartengono alla categoria catastale A/1 si distinguono rispetto alle abitazioni residenziali per la loro ubicazione in zone di pregio, oltre a un elevato grado di rifiniture',
            'A/2: abitazioni di tipo civile. Sono gli immobili di tipo residenziale, dove abita la stragrande maggioranza della popolazione',
            'A/3: abitazioni di tipo economico. Unità abitative costruite con materiali e rifiniture a basso costo. Esse presentano una rendita catastale minore, oltre a un prezzo d acquisto inferiore rispetto alle abitazioni che rientrano nella classe A/2',
            'A/4: abitazioni di tipo popolare. Livello ancora più basso rispetto agli immobili di tipo economico, sia a livello di rifiniture che di caratteristiche costruttive. Oggi non vengono più realizzate',
            'A/5: abitazioni di tipo ultrapopolare. Il catasto riporta la definizione di “realtà edilizie al di fuori degli standard minimi indispensabili”. Attraverso la nota del Ministero delle Finanze del 4 maggio 1994 (C1/1022/94), le classi A/5 e A/6 sono state annullate',
            'A/6: abitazioni di tipo rurale. Immobili a servizio di attività agricole (soppressa dal decreto del Ministero delle Finanze insieme alla classe A/5)',
            'A/7: abitazioni in villini. Fabbricati corrispondenti alle abitazioni di tipo civile ma con l’aggiunta di aree esterne ad uso esclusivo',
            'A/8: abitazioni in ville. Immobili con rifiniture e caratteristiche costruttive superiori all’ordinario, una superficie delle aree principali molto ampia e presenza di parco e/o giardino',
            'A/9: castelli, palazzi di eminenti pregi storici o artistici. Tali edifici si caratterizzano per una ripartizione dei volumi e degli spazi interni diversa da tutti gli altri immobili classificati nel catasto',
            'A/10: studi e uffici privati. Unità abitative destinate ad attività professionali. Tra queste rientrano le agenzie assicurative, i laboratori per analisi e quelli utilizzati dagli odontotecnici',
            'A/11: alloggi e abitazioni tipiche dei luoghi. L esempio classico di immobili classificati con la categoria A/11 è rappresentato dalla baita ad alta quota, ma si annoverano anche i trulli e gli stessi rifugi di montagna',
            'B/1: collegi, orfanotrofi, conventi, seminari, ricoveri, ospizi, caserme. Nella categoria catastale B/1 rientrano tutte quelle strutture senza fine di lucro adibite all’assistenza per anziani (ospizi), all’educazione dei minori (collegi, orfanotrofi), alla preparazione spirituale dei giovani seminaristi (seminari), all’ospitalità della comunità di un ordine mendicante (conventi), all’alloggio e all’attività delle forze armate (caserme)',
            'B/2: ospedali e case di cura senza fine di lucro. Sono strutture di ricovero e cura pubbliche, dunque senza fine di lucro',
            'B/3: riformatori e prigioni. La classe catastale B/3 include gli istituti penitenziari per adulti e minorenni',
            'B/4: uffici pubblici. Per uffici pubblici si intendono le sedi INPS, gli uffici dell’Agenzia delle Entrate e gli uffici territoriali della Camera di Commercio',
            'B/5: laboratori scientifici e scuole. Sono immobili realizzati per ospitare attività quali ricerca scientifica e istruzione.',
            'B/6: accademie, gallerie, musei, pinacoteche, biblioteche (a patto che non siano all’interno di palazzi di pregio e castelli appartenenti alla categoria A/9). Rientrano nella categoria catastale B/6 tutte le sedi culturali appena citate senza finalità di lucro',
            'B/7: oratori e cappelle non destinate all’esercizio pubblico del culto. Edifici costruiti per esercitare la religione (non in pubblico)',
            'B/8: magazzini sotterranei per depositi di derrate. Tutti i magazzini disposti a un livello inferiore rispetto al piano terra la cui funzione è quella di raccogliere le scorte',
            'C/1: locali commerciali. Gli immobili identificati dalla categoria catastale C/1 si occupano della vendita di prodotti, come ad esempio botteghe e negozi',
            'C/2: locali di deposito e magazzini. Strutture impiegate come locali di sgombero, deposito di merci e sottotetti',
            'C/3: laboratori per arti e mestieri. A differenza dei locali commerciali, gli edifici della classe catastale C/3 non sono destinati alla vendita dei prodotti, ma alla loro creazione o trasformazione da parte degli artigiani',
            'C/4: locali e fabbricati per esercizi sportivi senza fine di lucro. Tutti gli impianti privati in cui ci si allena o in cui va in scena un evento sportivo',
            'C/5: stabilimenti di acque curative e balneari senza fine di lucro. Così come la categoria C/4, si tratta di strutture private',
            'C/6: autorimesse, rimesse, scuderie e stalle senza fine di lucro. Strutture quali garage, posti macchina o box auto, ma anche scuderie e stalle',
            'C/7: tettoie aperte o chiuse. Qualsiasi struttura associata a gazebo o tettoia',
            'D/1: Opifici. Gli opifici sono le fabbriche, i capannoni, dove la materia prima viene lavorata e trasformata in prodotto',
            'D/2: alberghi con fine di lucro. Alberghi, hotel e le altre strutture ricettive dove i turisti soggiornano a pagamento',
            'D/3: sale per spettacoli e concerti, cinematografi e teatri con fine di lucro. Gli impianti in cui uno o più artisti si esibiscono davanti a un pubblico pagante',
            'D/4: ospedali e case di cura con fine di lucro. Strutture di ricovero e cura private, dove la prestazione medica viene offerta dietro pagamento',
            'D/5: istituti di assicurazione, cambio o credito con fine di lucro. La categoria catastale D/5 identifica banche e assicurazioni private',
            'D/6: locali e fabbricati per esercizi sportivi con fine di lucro. Stadi, palazzetti dello sport, piscine e tutti gli impianti sportivi dove il pubblico ha accesso pagando un biglietto',
            'D/7: fabbricati costruiti o adattati per le speciali esigenze di un’attività industriale e non suscettibili di destinazione diversa senza radicali trasformazioni. Strutture realizzate appositamente per adempiere a un’attività specifica, come ad esempio le stazioni di rifornimento',
            'D/8: fabbricati costituiti o adattati per le speciali esigenze di un’attività commerciale e non suscettibili di destinazione diversa senza radicali trasformazioni. Nella classe D/8 rientrano i centri commerciali',
            'D/9: edifici sospesi o galleggianti assicurati a punti fissi del suolo, ponti privati a pedaggio. Edifici, costruzioni che non dispongono di un loro suolo',
            'D/10: fabbricati rurali. In questa categoria rientrano la totalità dei vecchi fabbricati fuori dall’area urbana',
            'E/1: stazioni per servizi aerei, marittimi, terrestri e di trasporto. Alla classe catastale E/1 appartengono aeroporti, porti e stazioni ferroviarie',
            'E/2: ponti comunali e provinciali a pedaggio. Tutti i ponti pubblici per il cui passaggio è richiesto a ogni automobilista il pagamento di una tariffa fissa',
            'E/3: fabbricati e costruzioni per esigenze pubbliche speciali. Rientrano nella categoria strutture come chioschi ed edicole che vendono giornali',
            'E/4: recinti chiusi per esigenze pubbliche speciali. Sono inclusi i recinti che delimitano un’area dove si svolge, ad esempio, il mercato',
            'E/5: fabbricati costituenti fortificazioni e loro dipendenze. Tali fabbricati sono esenti in maniera permanente dal pagamento dell’IMU, come tutte le altre strutture presenti nel Gruppo E',
            'E/6: torri, semafori e fari per rendere d’uso pubblico l’orologio comunale. Anche per le strutture della categoria catastale E/6 vale lo stesso discorso, esenzione permanente dal pagamento dell’IMU',
            'E/7: fabbricati destinati all’esercizio pubblico dei culti. Nella classe catastale E/7 figurano gli edifici religiosi come cattedrali e chiese, al cui interno si celebrano le messe e le altre funzioni religiose',
            'E/8: costruzioni e fabbricati nei cimiteri, esclusi i sepolcri, i colombari e le tombe di famiglia. Strutture per cui vale l’esenzione dall’imposta municipale unica',
            'E/9: edifici a destinazione particolare non compresi nelle categorie precedenti del gruppo E.',
            'F/1: aree urbane. Rientrano nella categoria catastale F/1 tutte le aree situate al piano terra di fabbricati accatastati all’urbano',
            'F/2: unità collabenti. Strutture inutilizzabili, per cui non è concessa l’agibilità',
            'F/3: unità in corso di costruzione. Immobili che non sono stati terminati.',
            'F/4: unità in corso di definizione. Rispetto agli edifici della classe catastale F/3 la differenza è sottile ma importante. In questa categoria figurano gli immobili per cui non è stata stabilita né la destinazione d’uso né la consistenza',
            'F/5: lastrici solari. I lastrici solari sono, ad esempio, le terrazze o le aree libere collocate sopra immobili e rientrano tra le parti comuni di un condominio',
            'F/6: fabbricato in attesa di dichiarazione. Un qualsiasi fabbricato per cui non è ancora stata presentata la domanda di accatastamento',
            'F/7: infrastrutture di reti pubbliche di comunicazione. La categoria catastale F/7 identifica tutte quelle strutture realizzate dalle aziende di telecomunicazione per le reti pubbliche (vedi banda larga)',
        ];

        foreach ($descrizione as $category) {
            $c = new Categoria();
            $c->descrizione = $category;
            $c->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
