<?php

use App\Models\Tipo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('tipo')->nullable();
            $table->timestamps();
        });

        $tipi = [
            'APPARTAMENTO',
            'BOX',
            'CAPANNONE',
            'CASA IN CONDOMINIO',
            'CASA INDIPENDENTE',
            'EDIFICIO INDUSTRIALE',
            'VILLETTA',
            'VILLETTA A SCHIERA',
            'TERRENO AGRICOLO',
            'VILLA',
            'GARAGE',
            'FONDO COMMERCIALE',
            'RUDERE',
            'MASSERIA',
            'CASA CON CORTILE CONDIVISO',
        ];
        foreach ($tipi as $tip) {
            $t = new Tipo();
            $t->tipo = $tip;
            $t->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos');
    }
}
