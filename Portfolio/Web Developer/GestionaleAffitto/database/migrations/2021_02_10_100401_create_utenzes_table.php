<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUtenzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utenzes', function (Blueprint $table) {
            $table->id();
            $table->string('contatoregas')->nullable();
            $table->string('codiceclientegas')->nullable();
            $table->string('contatoreacqua')->nullable();
            $table->string('codiceclienteacqua')->nullable();
            $table->string('contatoreluce')->nullable();
            $table->string('codiceclienteluce')->nullable();
            $table->foreignId('proprieta_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('utenzes', function (Blueprint $table) {
            $table->dropForeign(['proprieta_id']);
        });
        Schema::dropIfExists('utenzes');
    }
}
