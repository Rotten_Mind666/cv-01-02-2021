<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticellasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('particellas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('foglio')->nullable();
            $table->string('particellaa')->nullable();
            $table->string('particellab')->nullable();
            $table->string('subalterno')->nullable();
            $table->text('nceu')->nullable();
            $table->foreignId('proprieta_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('particellas', function (Blueprint $table) {
            $table->dropForeign(['proprieta_id']);
        });
        Schema::dropIfExists('particellas');
    }
}
