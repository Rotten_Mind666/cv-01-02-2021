<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentificativosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identificativos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->integer('piano')->nullable();
            $table->string('scala')->nullable();
            $table->string('interno')->nullable();
            $table->foreignId('proprieta_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('identificativos', function (Blueprint $table) {
            $table->dropForeign(['proprieta_id']);
        });
        Schema::dropIfExists('identificativos');
    }
}
