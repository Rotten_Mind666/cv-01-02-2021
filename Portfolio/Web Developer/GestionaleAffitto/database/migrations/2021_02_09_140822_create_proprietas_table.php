<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProprietasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietas', function (Blueprint $table) {
            $table->id();
            $table->string('nomeProprieta')->nullable();
            $table->string('via')->nullable();
            $table->string('civico')->nullable();
            $table->string('cap')->nullable();
            $table->string('provincia')->nullable();
            $table->string('citta')->nullable();
            $table->foreignId('inquilino_id')->nullable()->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('categoria_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('tipo_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('utilizzo_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('proprietas', function (Blueprint $table) {
            $table->dropForeign(['inquilino_id']);
            $table->dropForeign(['categoria_id']);
            $table->dropForeign(['tipo_id']);
            $table->dropForeign(['utilizzo_id']);
        });
        Schema::dropIfExists('proprietas');
    }
}
