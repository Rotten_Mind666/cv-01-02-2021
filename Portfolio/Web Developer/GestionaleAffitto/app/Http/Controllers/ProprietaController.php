<?php

namespace App\Http\Controllers;

use App\Models\Tipo;
use App\Models\Altro;
use App\Models\Utenze;
use App\Models\Rendita;
use App\Models\Piantina;
use App\Models\Utilizzo;
use App\Models\Categoria;
use App\Models\Inquilino;
use App\Models\Proprieta;
use App\Models\Particella;
use Illuminate\Http\Request;
use App\Http\Requests\Requiem;
use App\Models\Identificativo;
use App\Exports\ProprietaExport;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class ProprietaController extends Controller
{

    public function index()
    {
        return view('welcome');
    }

    public function create()
    { 
        return view('proprieta.create');
    }

    public function createImmobile()
    { 
        $Inquilino = Inquilino::all();
        $Tipo = Tipo::all();
        $Utilizzo = Utilizzo::all();
        $Categoria = Categoria::all();
        return view('proprieta.createImmobile', compact('Categoria','Tipo','Utilizzo', 'Inquilino'));
    }

    // STORE
    public function store(Requiem $request)
    {
        $validated = $request->validated();

        if ($request->input('nome') != null) {
            if($request->file('documento')){
                $documento = $request->file('documento')->store('public/documenti');
            } else {
                $documento = 'public/default.jpg';
            }
            $inquilino = Inquilino::create ([
                'nome' => $request->input('nome'),
                'cognome' => $request->input('cognome'),
                'eta' => $request->input('eta'),
                'cf' => $request->input('cf'),
                'tel' => $request->input('tel'),
                'email' => $request->input('email'),
                'documento' => $documento,
            ]);
        }

        if ($request->input('nomeProprieta') == null) {
        } else {
            $proprieta = Proprieta::create([
                'nomeProprieta' => $request->input('nomeProprieta'),
                'categoria_id' => $request->input('descrizione'),
                'tipo_id' => $request->input('tipo'),
                'affittata' => $request->has('affittata'),
                'utilizzo_id' => $request->input('utilita'),
                'inquilino_id' => $request->input('inquilino_id'),
                'via' => $request->input('via'),
                'civico' => $request->input('civico'),
                'cap' => $request->input('cap'),
                'citta' => $request->input('citta'),
                'provincia' => $request->input('provincia'),
            ]);
            
            $identificativo = Identificativo::create ([
                'piano' => $request->input('piano'),
                'scala' => $request->input('scala'),
                'interno' => $request->input('interno'),
                'proprieta_id' => $proprieta->id,
            ]);
    
            if($request->file('pianta')){
                $pianta = $request->file('pianta')->store('public/documenti');
            } else {
                $pianta = 'storage/app/public/default.jpg';
            }
            $piantina = Piantina::create ([
                'mq' => $request->input('mq'),
                'anni' => $request->input('anni'),
                'vani' => $request->input('vani'),
                'pianta' => $pianta,
                'proprieta_id' => $proprieta->id,
            ]);
    
            $particella = Particella::create ([
                'foglio' => $request->input('foglio'),
                'particellaa' => $request->input('particellaa'),
                'particellab' => $request->input('particellab'),
                'subalterno' => $request->input('subalterno'),
                'nceu' => $request->input('nceu'),
                'proprieta_id' => $proprieta->id,
            ]);
    
            $altro = Altro::create ([
                'codicecomune' => $request->input('codicecomune'),
                'terrurb' => $request->input('terrurb'),
                'intpor' => $request->input('intpor'),
                'succ' => $request->input('succ'),
                'proprieta_id' => $proprieta->id,
            ]);
    
            $utenze = Utenze::create ([
                'contatoregas' => $request->input('contatoregas'),
                'codiceclientegas' => $request->input('codiceclientegas'),
                'contatoreacqua' => $request->input('contatoreacqua'),
                'codiceclienteacqua' => $request->input('codiceclienteacqua'),
                'contatoreluce' => $request->input('contatoreluce'),
                'codiceclienteluce' => $request->input('codiceclienteluce'),
                'proprieta_id' => $proprieta->id,
            ]);
    
            $rendite = Rendita::create ([
                'renditacatastale' => $request->input('renditacatastale'),
                'accatastamento' => $request->has('accatastamento'),
                'pigione' => $request->input('pigione'),
                'proprieta_id' => $proprieta->id,
            ]);
        }
        return redirect(route('welcome'));
    }
    
    // SHOW
    public function showImmobile(Proprieta $Proprieta)
    {
        $Inquilino = Inquilino::find($Proprieta->inquilino_id);
        $Altro = Altro::find($Proprieta->id);
        $Tipo = Tipo::find($Proprieta->tipo_id);
        $Utilizzo = Utilizzo::find($Proprieta->utilizzo_id);
        $Categoria = Categoria::find($Proprieta->categoria_id);
        $Identificativo = Identificativo::find($Proprieta->id);
        $Particella = Particella::find($Proprieta->id);
        $Piantina = Piantina::find($Proprieta->id);
        $Rendita = Rendita::find($Proprieta->id);
        $Utenze = Utenze::find($Proprieta->id);
        $tutto = ['Categoria','Utilizzo','Tipo','Altro','Proprieta','Identificativo','Inquilino','Particella','Piantina','Rendita','Utenze'];
        return view('proprieta.showImmobile', compact($tutto));
    }
    public function showInquilino(Inquilino $Inquilino)
    {
        $Proprieta = Proprieta::find('inquilino_id');
        return view('Proprieta.showInquilino', compact('Inquilino','Proprieta'));
    }

    // TABELLE
    // TABELLA INQUILINI
    public function tutto()
    {
        $Proprieta = Proprieta::all();
        $Inquilino = Inquilino::all();
        $Altro = Altro::all();
        $Tipo = Tipo::all();
        $Utilizzo = Utilizzo::all();
        $Categoria = Categoria::all();
        $Identificativo = Identificativo::all();
        $Particella = Particella::all();
        $Piantina = Piantina::all();
        $Rendita = Rendita::all();
        $Utenze = Utenze::all();
        $tutto = ['Categoria','Utilizzo','Tipo','Altro','Proprieta','Identificativo','Inquilino','Particella','Piantina','Rendita','Utenze'];
        return view('proprieta.tutto', compact($tutto));
    }
    // TABELLA IMMOBILI
    public function tuttoImmobili()
    {
        $Proprieta = Proprieta::all();
        $Inquilino = Inquilino::all();
        $Tipo = Tipo::all();
        $Utilizzo = Utilizzo::all();
        $Categoria = Categoria::all();
        $Altro = Altro::all();
        $Identificativo = Identificativo::all();
        $Particella = Particella::all();
        $Piantina = Piantina::all();
        $Rendita = Rendita::all();
        $Utenze = Utenze::all();
        $tutto = ['Categoria','Utilizzo','Tipo','Altro','Proprieta','Identificativo','Inquilino','Particella','Piantina','Rendita','Utenze'];
        return view('proprieta.tuttoImmobili', compact($tutto));
    }
    // FINE TABELLE

    public function editImmobile(Proprieta $Proprieta)
    {
        $Inquilino = Inquilino::all();
        $Tipo = Tipo::all();
        $Utilizzo = Utilizzo::all();
        $Categoria = Categoria::all();
        $Altro = Altro::find($Proprieta->id);
        $Identificativo = Identificativo::find($Proprieta->id);
        $Particella = Particella::find($Proprieta->id);
        $Piantina = Piantina::find($Proprieta->id);
        $Rendita = Rendita::find($Proprieta->id);
        $Utenze = Utenze::find($Proprieta->id);
        return view('Proprieta.editImmobile', compact('Proprieta','Categoria','Tipo','Utilizzo','Altro','Identificativo','Particella','Piantina','Rendita','Utenze','Inquilino'));
    }
    public function editInquilino(Inquilino $Inquilino)
    {
        return view('Proprieta.editInquilino', compact('Inquilino'));
    }

    // public function update(Requiem $request, Proprieta $Proprieta)
    // {
    //     // $Inquilino = Inquilino::find($Proprieta->inquilino_id);
    //     $Tipo = Tipo::find($Proprieta->tipo_id);
    //     $Categoria = Categoria::find($Proprieta->categoria_id);
    //     $Utilizzo = Utilizzo::find($Proprieta->utilizzo_id);
    //     $Altro = Altro::find($Proprieta->id);
    //     $Identificativo = Identificativo::find($Proprieta->id);
    //     $Particella = Particella::find($Proprieta->id);
    //     $Piantina = Piantina::find($Proprieta->id);
    //     $Rendita = Rendita::find($Proprieta->id);
    //     $Utenze = Utenze::find($Proprieta->id);

    //     // $Inquilino->update($request->all());
    //     $Tipo->update($request->all());
    //     $Categoria->update($request->all());
    //     $Utilizzo->update($request->all());
    //     $Altro->update($request->all());
    //     $Identificativo->update($request->all());
    //     $Particella->update($request->all());
    //     $Piantina->update($request->all());
    //     $Rendita->update($request->all());
    //     $Proprieta->update($request->all());
    //     $Utenze->update($request->all());
    //     return redirect(route('Proprieta.tuttoImmobili'));
    // }

    public function destroyProprieta(Proprieta $Proprieta)
    {   
        $Altro = Altro::find($Proprieta->id);
        $Identificativo = Identificativo::find($Proprieta->id);
        $Particella = Particella::find($Proprieta->id);
        $Piantina = Piantina::find($Proprieta->id);
        $Rendita = Rendita::find($Proprieta->id);
        $Utenze = Utenze::find($Proprieta->id);

        $Altro->delete();
        $Identificativo->delete();
        $Particella->delete();
        $Piantina->delete();
        $Rendita->delete();
        $Utenze->delete();
        $Proprieta->delete();
        return back();
    }


}
