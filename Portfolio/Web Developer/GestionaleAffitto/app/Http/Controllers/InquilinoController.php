<?php

namespace App\Http\Controllers;

use App\Models\Inquilino;
use Illuminate\Http\Request;

class InquilinoController extends Controller
{
    public function destroyInquilino(Inquilino $Inquilino)
    {
        $Inquilino->delete();
        return back();
    }
}
