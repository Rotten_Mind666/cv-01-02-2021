<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Requiem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'nome' => 'required',
            // 'cognome' => 'required',
            'eta' => 'numeric|min:18',
            'tel' => 'min:7',
            'piano' => 'numeric',
            'cap' => 'numeric',
            'mq' => 'numeric',
            'accatastamento' => 'boolean',
        ];
    }
}
