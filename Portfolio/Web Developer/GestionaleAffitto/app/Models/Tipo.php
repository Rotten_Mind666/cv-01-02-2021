<?php

namespace App\Models;

use App\Models\Proprieta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tipo extends Model
{
    use HasFactory;
    protected $guarded=[];
    
    public function proprieta(){
        return $this->hasMany(Proprieta::class);
    }
}
