<?php

namespace App\Models;

use App\Models\Proprieta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Categoria extends Model
{
    use HasFactory;
    protected $guarded=[];
    
    public function proprieta(){
        return $this->hasMany(Proprieta::class);
    }

    public function lAnteprima($anteprima) {
        $anteprima = strip_tags($anteprima);
        $anteprima = substr($anteprima, 0 , 200);
        $anteprima = $anteprima . "...";
        return $anteprima;
    }
    public function anteprima()
    {
        return $this->lAnteprima($this->descrizione);
    }
    public function lAnteprimaSm($anteprima) {
        $anteprima = strip_tags($anteprima);
        $anteprima = substr($anteprima, 0 , 5);
        return $anteprima;
    }
    public function anteprimaSm()
    {
        return $this->lAnteprimaSm($this->descrizione);
    }
}
