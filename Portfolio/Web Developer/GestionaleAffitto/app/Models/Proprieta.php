<?php

namespace App\Models;

use App\Models\Via;
use App\Models\Tipo;
use App\Models\Altro;
use App\Models\Utenze;
use App\Models\Rendita;
use App\Models\Piantina;
use App\Models\Utilizzo;
use App\Models\Categoria;
use App\Models\Inquilino;
use App\Models\Particella;
use App\Models\Identificativo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Proprieta extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function Inquilino(){
        return $this->belongsTo(Inquilino::class);
    }
    public function Categoria(){
        return $this->belongsTo(Categoria::class);
    }
    public function Tipo(){
        return $this->belongsTo(Tipo::class);
    }
    public function Utilizzo(){
        return $this->belongsTo(Utilizzo::class);
    }
    public function Altro(){
        return $this->hasOne(Altro::class);
    }
    public function Identificativo(){
        return $this->hasOne(Identificativo::class);
    }
    public function Particella(){
        return $this->hasOne(Particella::class);
    }
    public function Piantina(){
        return $this->hasOne(Piantina::class);
    }
    public function Rendita(){
        return $this->hasOne(Rendita::class);
    }
    public function Utenze(){
        return $this->hasOne(Utenze::class);
    }
    public function Via(){
        return $this->hasOne(Via::class);
    }
}
