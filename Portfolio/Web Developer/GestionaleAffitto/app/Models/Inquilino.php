<?php

namespace App\Models;

use App\Models\Proprieta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Inquilino extends Model
{
    use HasFactory;
    protected $guarded=[];
    
    public function Proprieta(){
        return $this->hasMany(Proprieta::class);
    }
}
