<?php

namespace App\Exports;

use App\Proprieta;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProprietaExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Proprieta::all();
    }
}
