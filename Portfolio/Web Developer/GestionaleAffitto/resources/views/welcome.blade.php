@extends('components.layout')
 
@section('content')
<div class="container my-5 py-5">
  <div class="row justify-content-center">
    <div class="col-12 col-md-6">      
        <a style="background-image: url('/public/img/Logo.png')" class="font-weight-bold" href="{{route('proprieta.create')}}" id="ahome"><i class="fa fa-user fa-5x"></i><i class="fa fa-plus fa-3x mr-4"></i> Aggiungi Inquilino</a>
    </div>
    <div class="col-12 col-md-6">      
        <a style="background-image: url('/public/img/Logo.png')" class="font-weight-bold" href="{{route('Proprieta.tutto')}}" id="ahome3"><i class="fas fa-list fa-3x"><i class="fa fa-user mr-4 p-1"></i></i> Lista Inquilini</a>
    </div>
    <div class="col-12 col-md-6">      
        <a style="background-image: url('/public/img/Logo.png')" class="font-weight-bold" href="{{route('proprieta.createImmobile')}}" id="ahome2"><i class="fas fa-building fa-5x"></i><i class="fa fa-plus fa-3x mr-4"></i> Aggiungi Immobile</a>
    </div>
    <div class="col-12 col-md-6">      
        <a style="background-image: url('/public/img/Logo.png')" class="font-weight-bold" href="{{route('Proprieta.tuttoImmobili')}}" id="ahome4"><i class="fas fa-list  fa-3x mr-4"><i class="fas fa-building p-1"></i></i> Lista Immobili</a>
    </div>
  </div>
</div>
@endsection

