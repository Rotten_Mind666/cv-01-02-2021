<div class="text-center mb-4">
    <a class="p-2 m-2 border border-white btn btn-lg btn-dark" href="{{route('welcome')}}">Proprietà</a>
    <a class="p-2 m-2 border border-white btn btn-lg btn-dark" href="{{route('proprieta.create')}}">Aggiungi un nuovo Inquilino <i class="fa fa-user" aria-hidden="true"></i></a>
    <a class="p-2 m-2 border border-white btn btn-lg btn-dark" href="{{route('proprieta.createImmobile',)}}">Aggiungi un nuovo Immobile <i class="fa fa-home" aria-hidden="true"></i></a>
    <a href="{{route('Proprieta.tutto')}}" class="p-2 m-2 border border-white btn btn-lg btn-dark">Tabella Inquilini <i class="fas fa-table"></i></a>
    <a href="{{route('Proprieta.tuttoImmobili')}}" class="p-2 m-2 border border-white btn btn-lg btn-dark">Tabella Immobili <i class="fas fa-table"></i></a>
 </div>