@extends('components.layout')
@section('content')
<x-widget/>

<div class="container">
    <div class="row">
        <div class="col-12">
            <form action="{{route('update', $Proprieta )}}" method="post">
                @csrf                
                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Questo Immobile ha un Inquilino?</h2>
                    {{-- INQUILINO --}}
                    <span><i>*se hai già creato un inquilino aggiungilo dalla lista.</i></span><br>
                    <label for="inquilino_id">Inquilino:</label>
                    <select class="form-control text-capitalize" type="text" name="inquilino_id" id="inquilino_id">
                        @if ($Proprieta->inquilino_id == null)
                            @foreach ($Inquilino as $item)
                                <option value="{{$item->id}}">nome: {{$item->nome}} | cognome: {{$item->cognome}} | età: {{$item->eta}} | codice fiscale: {{$item->cf}}</option>
                            @endforeach
                        @else
                                <option {{$o = $Proprieta->inquilino_id -1}} value="{{$o}}">nome: {{$Inquilino[$o]->nome}} | cognome: {{$Inquilino[$o]->cognome}} | età: {{$Inquilino[$o]->eta}} | codice fiscale: {{$Inquilino[$o]->cf}} (inquilino attuale)</option>
                            @foreach ($Inquilino as $item)
                                <option value="{{$item->id}}">nome: {{$item->nome}} | cognome: {{$item->cognome}} | età: {{$item->eta}} | codice fiscale: {{$item->cf}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Nome Immobile</h2>
                    {{-- NOME PROPRIETA --}}
                    <label for="nomeProprieta">Nome Immobile:</label>
                    <input value="{{$Proprieta->nomeProprieta}}" class="form-control" type="text" name="nomeProprieta" id="nomeProprieta">
                </div>
                <div class="form-group p-2 text-center border">
                    {{-- AFFITATA PROPRIETA --}}
                    <label for="affittata">E' affittata?</label>
                    @if($Proprieta->affittata == 1)
                        <input class="form-control" type="checkbox" name="affittata" value="1" id="affittata" checked>
                    @else
                        <input class="form-control" type="checkbox" name="affittata" value="0" id="affittata">
                    @endif
                    {{-- AFFITATA PROPRIETA --}}
                    {{-- ACCATASTAMENTO --}}
                    <label for="accatastamento">In Via di Accatastamento?</label>
                    @if($Rendita->accatastamento == 1)
                        <input class="form-control" type="checkbox" name="accatastamento" value="1" id="accatastamento" checked>
                    @else
                        <input class="form-control" type="checkbox" name="accatastamento" value="0" id="accatastamento">
                    @endif
                    {{-- ACCATASTAMENTO --}}
                </div>
                


                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Dati Immobile</h2>
                    {{-- STRADA --}}
                    <label for="via">Via Immobile:</label>
                    <input value="{{$Proprieta->via}}"  class="form-control" type="text" name="via" id="via">
                    <label for="civico">Civico Immobile:</label>
                    <input value="{{$Proprieta->civico}}"  class="form-control" type="text" name="civico" id="civico">
                    <label for="cap">CAP Immobile:</label>
                    <input value="{{$Proprieta->cap}}"  class="form-control" type="number" name="cap" id="cap">
                    <label for="provincia">Provincia Immobile:</label>
                    <input value="{{$Proprieta->provincia}}"  class="form-control" type="text" name="provincia" id="provincia">
                    <label for="citta">Citta Immobile:</label>
                    <input value="{{$Proprieta->citta}}"  class="form-control" type="text" name="citta" id="citta">
                    {{-- STRADA--}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Interno Immobile</h2>
                    {{-- ID --}}
                    <label for="piano">Piano:</label>
                    <input value="{{$Identificativo->piano}}"  class="form-control" type="number" name="piano" id="piano">
                    <label for="scala">Scala:</label>
                    <input value="{{$Identificativo->scala}}"  class="form-control" type="text" name="scala" id="scala">
                    <label for="interno">Interno:</label>
                    <input value="{{$Identificativo->interno}}"  class="form-control" type="text" name="interno" id="interno">
                    {{-- ID --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Piantina Immobile</h2>
                    {{-- PIANTINA --}}
                    <label for="mq">MQ Immobile:</label>
                    <input value="{{$Piantina->mq}}"  class="form-control" type="number" name="mq" id="mq">
                    <label for="vani">Vani Immobile:</label>
                    <input value="{{$Piantina->vani}}"  class="form-control" type="text" name="vani" id="vani">
                    <label for="anni">Anni Immobile:</label>
                    <input value="{{$Piantina->anni}}"  class="form-control" type="number" name="anni" id="anni">
                    {{-- PIANTINA --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Catasto Immobile</h2>
                    {{-- CATASTO --}}
                    <label for="pianta">Planimetria (file):</label>
                    <input value="{{$Particella->pianta}}"  class="form-control" type="file" name="pianta" id="pianta">
                    <label for="foglio">Foglio:</label>
                    <input value="{{$Particella->foglio}}"  class="form-control" type="text" name="foglio" id="foglio">
                    <label for="particellaa">Particella A:</label>
                    <input value="{{$Particella->particellaa}}"  class="form-control" type="text" name="particellaa" id="particellaa">
                    <label for="particellab">Particella B:</label>
                    <input value="{{$Particella->particellab}}"  class="form-control" type="text" name="particellab" id="particellab">
                    <label for="subalterno">Subalterno:</label>
                    <input value="{{$Particella->subalterno}}"  class="form-control" type="text" name="subalterno" id="subalterno">
                    <label for="nceu">NCEU Nuovo Catasto Edilizio Urbano</label>
                    <input value="{{$Particella->nceu}}"  class="form-control" type="text" name="nceu" id="nceu">
                    {{-- CATASTO --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Altri Dati Immobile:</h2>
                    {{-- SPECIFICHE --}}
                    <label for="codicecomune">Codice Comune</label>
                    <input value="{{$Altro->codicecomune}}"  class="form-control" type="text" name="codicecomune" id="codicecomune">
                    <label for="terrurb">Terreno / Urbano</label>
                    <select class="form-control" type="text" name="terrurb" id="terrurb">
                        <option value="Terreno">Terreno</option>
                        <option value="Urbano">Urbano</option>
                    </select>
                    <label for="intpor">Intero / Porzione</label>
                    <select class="form-control" type="text" name="intpor" id="intpor">
                        <option value="Intero">Intero</option>
                        <option value="Porzione">Porzione</option>
                    </select>
                    <label for="succ">Sezione urbana / Comune Catastale</label>
                    <input value="{{$Altro->succ}}"  class="form-control" type="text" name="succ" id="succ">
                    {{-- SPECIFICHE --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Tipo Immobile</h2>
                    {{-- CATEGORIA --}}
                    <label for="descrizione">Categoria Catastale:</label>
                    <select class="form-control" type="text" name="descrizione" id="descrizione">
                            <option value="{{$l = $Proprieta->categoria_id -1}}" selected>{{$Categoria[$l]->descrizione}}</option>
                        @foreach ($Categoria as $item)
                            <option value="{{$item->id}}">{{$item->anteprima()}}</option>
                        @endforeach
                    </select>
                    {{-- CATEGORIA --}}
                    {{-- TIPO --}}
                    <label for="tipo">Tipo di immobile:</label>
                    <select class="form-control" type="text" name="tipo" id="tipo">
                            <option value="{{$l = $Proprieta->tipo_id -1}}" selected>{{$Tipo[$l]->tipo}}</option>
                        @foreach ($Tipo as $item)
                            <option value="{{$item->id}}">{{$item->tipo}}</option>
                        @endforeach
                    </select>
                    {{-- TIPO --}}

                    {{-- UTILIZZO --}}
                    <label for="utilita">Utilizzo del immobile:</label>
                    <select class="form-control" type="text" name="utilita" id="utilita">
                            <option value="{{$l = $Proprieta->utilizzo_id -1}}" selected>{{$Utilizzo[$l]->utilita}}</option>
                        @foreach ($Utilizzo as $item2)
                            <option value="{{$item2->id}}">{{$item2->utilita}}</option>
                        @endforeach
                    </select>
                    {{-- UTILIZZO --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Utenze Immobile:</h2>
                    {{-- GAS --}}
                    <label for="contatoregas">Numero Contatore Gas:</label>
                    <input value="{{$Utenze->contatoregas}}"  class="form-control" type="text" name="contatoregas" id="contatoregas">
                    <label for="codiceclientegas">Codice Cliente Gas:</label>
                    <input value="{{$Utenze->codiceclientegas}}"  class="form-control" type="text" name="codiceclientegas" id="codiceclientegas">
                    {{-- GAS --}}
                    
                    {{-- ACQUA --}}
                    <label for="contatoreacqua">Numero Contatore Acqua:</label>
                    <input value="{{$Utenze->contatoreacqua}}"  class="form-control" type="text" name="contatoreacqua" id="contatoreacqua">
                    <label for="codiceclienteacqua">Codice Cliente Acqua:</label>
                    <input value="{{$Utenze->codiceclienteacqua}}"  class="form-control" type="text" name="codiceclienteacqua" id="codiceclienteacqua">
                    {{-- ACQUA --}}
                    
                    {{-- LUCE --}}
                    <label for="contatoreluce">Numero Contatore Luce:</label>
                    <input value="{{$Utenze->contatoreluce}}"  class="form-control" type="text" name="contatoreluce" id="contatoreluce">
                    <label for="codiceclienteluce">Codice Cliente Luce:</label>
                    <input value="{{$Utenze->codiceclienteluce}}"  class="form-control" type="text" name="codiceclienteluce" id="codiceclienteluce">
                    {{-- LUCE --}}
                </div>
                    
                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Rendita Immobile:</h2>
                    {{-- RENDITA CATASTALE --}}
                    <label for="renditacatastale">Rendita Catastale:</label>
                    <input value="{{$Rendita->renditacatastale}}"  class="form-control" type="number" name="renditacatastale" id="renditacatastale">
                    {{-- RENDITA CATASTALE --}}
                    
                    {{-- PIGIONE --}}
                    <label for="pigione">Pigione Al Mese:</label>
                    <input value="{{$Rendita->pigione}}"  class="form-control" type="number" name="pigione" id="pigione">
                    {{-- PIGIONE --}}
                </div>

                <button type="submit" class="btn btn-primary btn-lg btn-block">Crea Immobile</button>
            </form>
        </div>
    </div>
</div>
@endsection