@extends('components.layout')
@section('content')
<x-widget/>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row">
        <div class="col-12">
            <form action="{{route('update', $Inquilino)}}" method="post">
                @csrf
                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Modifica Inquilino</h2>
                    {{-- INQUILINO--}}
                    <label for="nome">Nome:</label>
                    <input value="{{$Inquilino->nome}}" class="form-control" type="text" name="nome" id="nome">
                    <label for="cognome">Cognome:</label>
                    <input value="{{$Inquilino->cognome}}" class="form-control" type="text" name="cognome" id="cognome">
                    <label for="cf">Codice Fiscale:</label>
                    <input value="{{$Inquilino->cf}}" class="form-control" type="text" name="cf" id="cf">
                    <label for="eta">Eta:</label>
                    <input value="{{$Inquilino->eta}}" class="form-control" type="number" name="eta" id="eta">
                    <label for="documento">Documento:</label>
                    <input value="{{$Inquilino->documento}}" class="form-control" type="file" name="documento" id="documento">
                    <label for="tel">Tel/Cell:</label>
                    <input value="{{$Inquilino->tel}}" class="form-control" type="tel" name="tel" id="tel">
                    <label for="email">Email:</label>
                    <input value="{{$Inquilino->email}}" class="form-control" type="email" name="email" id="email">
                    {{-- <label for="domicilio">Citta:</label>
                    <input class="form-control" type="text" name="domicilio" id="domicilio">
                    <label for="viad">Via:</label>
                    <input class="form-control" type="text" name="viad" id="viad">
                    <label for="civicod">Civico:</label>
                    <input class="form-control" type="text" name="civicod" id="civicod">
                    <label for="residente">Residente a:</label>
                    <input class="form-control" type="text" name="residente" id="residente">
                    <label for="viar">Via Residenza:</label>
                    <input class="form-control" type="text" name="viar" id="viar">
                    <label for="civicor">Civico Residenza:</label>
                    <input class="form-control" type="text" name="civicor" id="civicor"> --}}
                    {{-- INQUILINO --}}
                </div>
                <button type="submit" class="btn btn-warning btn-lg btn-block">Applica Modifica</button>
            </form>
        </div>
    </div>
</div>
@endsection