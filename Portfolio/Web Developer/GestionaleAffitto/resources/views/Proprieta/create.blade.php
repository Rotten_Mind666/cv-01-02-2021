@extends('components.layout')

@section('content')
<x-widget/>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row">
        <div class="col-12">
            <form action="{{route('store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Dati Inquilino (opzionale)</h2>
                    {{-- INQUILINO--}}
                    <label for="nome">Nome:</label>
                    <input class="form-control" type="text" name="nome" id="nome">
                    <label for="cognome">Cognome:</label>
                    <input class="form-control" type="text" name="cognome" id="cognome">
                    <label for="cf">Codice Fiscale:</label>
                    <input class="form-control" type="text" name="cf" id="cf">
                    <label for="eta">Eta:</label>
                    <input class="form-control" type="number" name="eta" id="eta">
                    <label for="documento">Documento:</label>
                    <input class="form-control" type="file" name="documento" id="documento">
                    <label for="tel">Tel/Cell:</label>
                    <input class="form-control" type="tel" name="tel" id="tel">
                    <label for="email">Email:</label>
                    <input class="form-control" type="email" name="email" id="email">
                    {{-- INQUILINO --}}
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Crea Inquilino</button>
            </form>
        </div>
        
    </div>
</div>
@endsection