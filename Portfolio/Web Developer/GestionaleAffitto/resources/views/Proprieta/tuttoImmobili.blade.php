@extends('components.layout')

@section('content')
<x-widget/>

    <table class="table table-dark text-center table-responsive-lg">
        <thead> 
            <tr class="text-uppercase"> 
                <th id="id" scope="col">#</th>
                <th id="nomeP" scope="col">Nome Casa</th>
                <th id="via" scope="col">Via</th>
                <th id="civico" scope="col">Civico</th>
                <th id="citta" scope="col">Citta</th>
                <th id="affittata" scope="col">Affittata</th>
                <th id="inquilino" scope="col">Inquilino</th>
                {{-- <th id="edit" scope="col">Modifica</th> --}}
                <th id="delete" scope="col">Elimina</th>
                <th id="show" scope="col">Vedi dettagli</th>
                {{-- <th id="rendita" scope="col">Rendita Catastale</th> --}}
                {{-- <th id="accatastamento" scope="col">In accatastamento</th> --}}
                {{-- <th id="pigione" scope="col">Pigione</th> --}}
                {{-- <th id="contatoreg" scope="col">Contatore Gas</th> --}}
                {{-- <th id="codiceg" scope="col">Codice cliente Gas</th> --}}
                {{-- <th id="contatorea" scope="col">Contatore Acqua</th> --}}
                {{-- <th id="codicea" scope="col">Codice Cliente Acqua</th> --}}
                {{-- <th id="contatorel" scope="col">Contatore Luce</th> --}}
                {{-- <th id="codicel" scope="col">Codice Cliente Luce</th> --}}
                {{-- <th id="codicec" scope="col">Codice Comune</th> --}}
                {{-- <th id="terr" scope="col">Terreno/Urbano</th> --}}
                {{-- <th id="inter" scope="col">Intero/Porzione</th> --}}
                {{-- <th id="succ" scope="col">Sezione Urbana/Comune Catastale</th> --}}
                {{-- <th id="foglio" scope="col">Foglio</th> --}}
                {{-- <th id="particellaa" scope="col">Particella A</th> --}}
                {{-- <th id="particellab" scope="col">Particella B</th> --}}
                {{-- <th id="sub" scope="col">Subalterno</th> --}}
                {{-- <th id="nceu" scope="col">Nuovo_Catasto Edilizio Urbano</th> --}}
                {{-- <th id="mq" scope="col">MQ</th> --}}
                {{-- <th id="anni" scope="col">Anni</th> --}}
                {{-- <th id="vani" scope="col">Vani</th> --}}
                {{-- <th id="pianta" scope="col">Pianta</th> --}}
                {{-- <th id="cap" scope="col">Cap</th> --}}
                {{-- <th id="provincia" scope="col">Provincia</th> --}}
            </tr>
        </thead>
        <tbody>
            @foreach ($Proprieta as $item)
            <tr>
                <td id="id">{{$item->id}}</td>
                <td id="nomeP">{{$item->nomeProprieta}}</td>
                <td id="via"><a class="text-decoration-none text-white text-capitalize" href="https://www.google.it/maps/place/{{$item->via}}"><u>{{$item->via}}</u></a></td>
                <td id="civico">{{$item->civico}}</td>
                <td id="via"><a class="text-decoration-none text-white font-weight-bold text-capitalize" href="https://www.google.it/maps/place/{{$item->citta}}">{{$item->citta}}</a></td>
                    @if ($item->inquilino_id != null)
                        <td class="text-success" id="affittata">SI</td>
                    @else
                        <td class="text-danger" id="affittata">NO</td>
                    @endif

                    @if ($item->inquilino_id == null)
                        <td> - </td>
                    @else
                        <span class="d-none">{{$w = $item->inquilino_id -1}}</span>
                        <td>{{$Inquilino[$w]->nome}}</td>
                    @endif

                    {{-- AZIONI --}}
                    {{-- <td><a href="{{route('Proprieta.editImmobile', $item)}}" class="btn btn-outline-warning" id="edit">MODIFICA</a></td> --}}
                    <td><form action="{{route('Proprieta.destroyProprieta', $item)}}" method="post">@csrf @method('DELETE') <button type="submit" class="btn btn-outline-danger">ELIMINA</button></form></a></td>
                    <td id="show"><a href="{{route('Proprieta.showImmobile', $item)}}" class="btn btn-outline-white">VEDI IMMOBILE</a></td>
            </tr>
            @endforeach
            {{-- @if ($Proprieta->)
                
            @endif --}}
                {{-- @foreach ($Rendita as $item) --}}
                    {{-- <td id="rendita">{{$item->renditacatastale}} €</td> --}}
                    {{-- @if ($item->accatastamento == 1) --}}
                    {{-- <td class="text-success" id="accatastamento">✔</td> --}}
                    {{-- @else --}}
                    {{-- <td class="text-danger" id="accatastamento">X</td> --}}
                    {{-- @endif --}}
                    {{-- <td id="pigione">{{$item->pigione}}</td> --}}
                {{-- @endforeach --}}
                {{-- @foreach ($Utenze as $item) --}}
                    {{-- <td id="contatoreg">{{$item->contatoregas}} #</td> --}}
                    {{-- <td id="codiceg">{{$item->codiceclientegas}}</td> --}}
                    {{-- <td id="contatorea">{{$item->contatoreacqua}} #</td> --}}
                    {{-- <td id="codicea">{{$item->codiceclienteacqua}}</td> --}}
                    {{-- <td id="contatorel">{{$item->contatoreluce}} #</td> --}}
                    {{-- <td id="codicel">{{$item->codiceclienteluce}}</td> --}}
                {{-- @endforeach --}}
                {{-- @foreach ($Altro as $item) --}}
                    {{-- <td id="codicec">{{$item->codicecomune}}</td> --}}
                    {{-- <td id="terr">{{$item->terrurb}}</td> --}}
                    {{-- <td id="inter">{{$item->intpor}}</td> --}}
                    {{-- <td id="succ">{{$item->succ}}</td> --}}
                {{-- @endforeach --}}
                {{-- @foreach ($Particella as $item) --}}
                    {{-- <td id="foglio">{{$item->foglio}}</td> --}}
                    {{-- <td id="particellaa">{{$item->particellaa}}</td> --}}
                    {{-- <td id="particellab">{{$item->particellab}}</td> --}}
                    {{-- <td id="sub">{{$item->subalterno}}</td> --}}
                    {{-- <td id="nceu">{{$item->nceu}}</td> --}}
                {{-- @endforeach --}}
                {{-- @foreach ($Piantina as $item) --}}
                    {{-- <td id="mq">{{$item->mq}}</td> --}}
                    {{-- <td id="anni">{{$item->anni}}</td> --}}
                    {{-- <td id="vani">{{$item->vani}}</td> --}}
                    {{-- <td id="pianta">{{$item->pianta}}</td> --}}
                {{-- @endforeach --}}
        </tbody>
    </table>
@endsection