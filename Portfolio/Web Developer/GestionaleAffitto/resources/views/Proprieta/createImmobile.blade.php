@extends('components.layout')

@section('content')
<x-widget/>

<div class="container">
    <div class="row">
        <div class="col-12">
            <form action="{{route('store')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Questo Immobile ha un Inquilino?</h2>
                    {{-- INQUILINO --}}
                    <span><i>*se hai già creato un inquilino aggiungilo dalla lista.</i></span><br>
                    <label for="inquilino_id">Nome Immobile:</label>
                    <select class="form-control text-capitalize" type="text" name="inquilino_id" id="inquilino_id">
                            <option value="0" selected disabled>lista inquilini [...]</option>
                        @foreach ($Inquilino as $item)
                            <option value="{{$item->id}}">nome: {{$item->nome}} | cognome: {{$item->cognome}} | età: {{$item->eta}} | codice fiscale: {{$item->cf}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Nome Immobile</h2>
                    {{-- NOME PROPRIETA --}}
                    <label for="nomeProprieta">Nome Immobile:</label>
                    <input class="form-control" type="text" name="nomeProprieta" id="nomeProprieta">
                </div>
                <div class="form-group p-2 text-center border">
                    {{-- AFFITATA PROPRIETA --}}
                    <label for="affittata">E' affittata?</label>
                    <input class="form-control" type="checkbox" value="0" name="affittata" id="affittata">
                    {{-- AFFITATA PROPRIETA --}}
                    {{-- ACCATASTAMENTO --}}
                    <label for="accatastamento">In Via di Accatastamento?</label>
                    <input class="form-control" type="checkbox" value="0" name="accatastamento" id="accatastamento">
                    {{-- ACCATASTAMENTO --}}
                </div>
                
                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Tipo Immobile</h2>
                    {{-- CATEGORIA --}}
                    <label for="descrizione">Categoria Catastale:</label>
                    <select class="form-control" type="text" name="descrizione" id="descrizione">
                        @foreach ($Categoria as $item)
                            <option value="{{$item->id}}">{{$item->anteprima()}}</option>
                        @endforeach
                    </select>
                    {{-- CATEGORIA --}}
                
                    {{-- TIPO --}}
                    <label for="tipo">Tipo di immobile:</label>
                    <select class="form-control" type="text" name="tipo" id="tipo">
                        @foreach ($Tipo as $item)
                            <option value="{{$item->id}}">{{$item->tipo}}</option>
                        @endforeach
                    </select>
                    {{-- TIPO --}}

                    {{-- UTILIZZO --}}
                    <label for="utilita">Utilizzo del immobile:</label>
                    <select class="form-control" type="text" name="utilita" id="utilita">
                        @foreach ($Utilizzo as $item2)
                            <option value="{{$item2->id}}">{{$item2->utilita}}</option>
                        @endforeach
                    </select>
                    {{-- UTILIZZO --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Dati Immobile</h2>
                    {{-- STRADA --}}
                    <label for="via">Via Immobile:</label>
                    <input class="form-control" type="text" name="via" id="via">
                    <label for="civico">Civico Immobile:</label>
                    <input class="form-control" type="text" name="civico" id="civico">
                    <label for="cap">CAP Immobile:</label>
                    <input class="form-control" type="number" name="cap" id="cap">
                    <label for="provincia">Provincia Immobile:</label>
                    <input class="form-control" type="text" name="provincia" id="provincia">
                    <label for="citta">Citta Immobile:</label>
                    <input class="form-control" type="text" name="citta" id="citta">
                    {{-- STRADA--}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Interno Immobile</h2>
                    {{-- ID --}}
                    <label for="piano">Piano:</label>
                    <input class="form-control" type="number" name="piano" id="piano">
                    <label for="scala">Scala:</label>
                    <input class="form-control" type="text" name="scala" id="scala">
                    <label for="interno">Interno:</label>
                    <input class="form-control" type="text" name="interno" id="interno">
                    {{-- ID --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Piantina Immobile</h2>
                    {{-- PIANTINA --}}
                    <label for="mq">MQ Immobile:</label>
                    <input class="form-control" type="number" name="mq" id="mq">
                    <label for="vani">Vani Immobile:</label>
                    <input class="form-control" type="text" name="vani" id="vani">
                    <label for="anni">Anni Immobile:</label>
                    <input class="form-control" type="number" name="anni" id="anni">
                    {{-- PIANTINA --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Catasto Immobile</h2>
                    {{-- CATASTO --}}
                    <label for="pianta">Planimetria (file):</label>
                    <input class="form-control" type="file" name="pianta" id="pianta">
                    <label for="foglio">Foglio:</label>
                    <input class="form-control" type="text" name="foglio" id="foglio">
                    <label for="particellaa">Particella A:</label>
                    <input class="form-control" type="text" name="particellaa" id="particellaa">
                    <label for="particellab">Particella B:</label>
                    <input class="form-control" type="text" name="particellab" id="particellab">
                    <label for="subalterno">Subalterno:</label>
                    <input class="form-control" type="text" name="subalterno" id="subalterno">
                    {{-- CATASTO --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Altri Dati Immobile:</h2>
                    {{-- SPECIFICHE --}}
                    <label for="nceu">NCEU Nuovo Catasto Edilizio Urbano</label>
                    <input class="form-control" type="text" name="nceu" id="nceu">
                    <label for="codicecomune">Codice Comune</label>
                    <input class="form-control" type="text" name="codicecomune" id="codicecomune">
                    <label for="terrurb">Terreno / Urbano</label>
                    <select class="form-control" type="text" name="terrurb" id="terrurb">
                        <option value="Terreno">Terreno</option>
                        <option value="Urbano">Urbano</option>
                    </select>
                    <label for="intpor">Intero / Porzione</label>
                    <select class="form-control" type="text" name="intpor" id="intpor">
                        <option value="Intero">Intero</option>
                        <option value="Porzione">Porzione</option>
                    </select>
                    <label for="succ">Sezione urbana / Comune Catastale</label>
                    <input class="form-control" type="text" name="succ" id="succ">
                    {{-- SPECIFICHE --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Utenze Immobile:</h2>
                    {{-- GAS --}}
                    <label for="contatoregas">Numero Contatore Gas:</label>
                    <input class="form-control" type="text" name="contatoregas" id="contatoregas">
                    <label for="codiceclientegas">Codice Cliente Gas:</label>
                    <input class="form-control" type="text" name="codiceclientegas" id="codiceclientegas">
                    {{-- GAS --}}
                    
                    {{-- ACQUA --}}
                    <label for="contatoreacqua">Numero Contatore Acqua:</label>
                    <input class="form-control" type="text" name="contatoreacqua" id="contatoreacqua">
                    <label for="codiceclienteacqua">Codice Cliente Acqua:</label>
                    <input class="form-control" type="text" name="codiceclienteacqua" id="codiceclienteacqua">
                    {{-- ACQUA --}}
                    
                    {{-- LUCE --}}
                    <label for="contatoreluce">Numero Contatore Luce:</label>
                    <input class="form-control" type="text" name="contatoreluce" id="contatoreluce">
                    <label for="codiceclienteluce">Codice Cliente Luce:</label>
                    <input class="form-control" type="text" name="codiceclienteluce" id="codiceclienteluce">
                    {{-- LUCE --}}
                </div>

                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Rendita Immobile:</h2>
                    {{-- RENDITA CATASTALE --}}
                    <label for="renditacatastale">Rendita Catastale:</label>
                    <input class="form-control" type="number" name="renditacatastale" id="renditacatastale">
                    {{-- RENDITA CATASTALE --}}
                    
                    {{-- PIGIONE --}}
                    <label for="pigione">Pigione Al Mese:</label>
                    <input class="form-control" type="number" name="pigione" id="pigione">
                    {{-- PIGIONE --}}
                </div>

                <button type="submit" class="btn btn-primary btn-lg btn-block">Crea Immobile</button>
            </form>
        </div>
    </div>
</div>
@endsection