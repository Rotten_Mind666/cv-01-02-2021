@extends('components.layout')
@section('content')
<x-widget/>
<div class="container my-5 py-5">
  <div class="row">
    <div class="col-12">
        <h1>{{$Proprieta->nomeProprieta}}</h1>
        <div class="row">
            
            <table class="table table-dark text-center table-responsive-lg table-hover table-bordered">
                <thead> 
                    <tr class="text-uppercase"> 
                        <th>Via:</th>
                        <th>Civico:</th>
                        <th>Citta:</th>
                        <th>Utilizzo:</th>
                        <th>Accatastata:</th>
                        <th>Pigione:</th>
                        <th>Rendita Catastale:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-secondary text-dark">
                        <td>{{$Proprieta->via}}</td>
                        <td>{{$Proprieta->civico}}</td>
                        <td>{{$Proprieta->citta}}</td>
                        <td>{{$Utilizzo->utilita}}</td>
                        @if ($Rendita->accatastamento == 1)
                            <td><span class="text-success">SI</span></td>
                        @else
                            <td><span class="text- danger">NO</span></td>
                        @endif
                        <td>{{$Rendita->pigione}}€</td>
                        <td>{{$Rendita->renditacatastale}}€</td>
                    </tr>
                </tbody>
                <thead> 
                    <tr class="text-uppercase"> 
                        <th>Cap:</th>
                        <th>Provincia:</th>
                        <th>Categoria:</th>
                        <th>Piano:</th>
                        <th>Scala:</th>
                        <th>Interno:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-secondary text-dark">
                        <td>{{$Proprieta->cap}}</td>
                        <td>{{$Proprieta->provincia}}</td>
                        <td>{{$Categoria->anteprimaSm()}}</td>
                        <td>{{$Identificativo->piano}}</td>
                        <td>{{$Identificativo->scala}}€</td>
                        <td>{{$Identificativo->interno}}€</td>
                    </tr>
                </tbody>
                <thead> 
                    <tr class="text-uppercase"> 
                        <th>Metri Quadri:</th>
                        <th>Anni:</th>
                        <th>Vani:</th>
                        <th>Piantina:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-secondary text-dark">
                        <td>{{$Piantina->mq}}</td>
                        <td>{{$Piantina->anni}}</td>
                        <td>{{$Piantina->vani}}</td>
                        @if(strlen($Piantina->pianta) < 31) <td class="text-danger">Nessuna Piantina</td>
                        @else <td><a href="{{$Piantina->pianta}}">Visualizza Piantina</a></td>
                        @endif
                    </tr>
                </tbody>
                <thead> 
                    <tr class="text-uppercase"> 
                        <th>foglio:</th>
                        <th>particellaa:</th>
                        <th>particellab:</th>
                        <th>subalterno:</th>
                        <th>nceu:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-secondary text-dark">
                        <td>{{$Particella->foglio}}</td>
                        <td> {{$Particella->particellaa}}</td>
                        <td> {{$Particella->particellab}}</td>
                        <td>{{$Particella->subalterno}}</td>
                        <td>{{$Particella->nceu}}</td>
                    </tr>
                </tbody>
                <thead> 
                    <tr class="text-uppercase"> 
                        <th>Codice Comune:</th>
                        <th>Genere:</th>
                        <th>Porzione:</th>
                        <th>Sezione:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-secondary text-dark">
                        <td>{{$Altro->codicecomune}}</td>
                        <td>{{$Altro->terrurb}}</td>
                        <td>{{$Altro->intpor}}</td>
                        <td>{{$Altro->succ}}</td>
                    </tr>
                </tbody>
                <thead> 
                    <tr class="text-uppercase"> 
                        <th>Codice Contatore Gas:</th>
                        <th>Codice Cliente Gas:</th>
                        <th>Codice Contatore Acqua:</th>
                        <th>Codice Cliente Acqua:</th>
                        <th>Codice Contatore Luce:</th>
                        <th>Codice Cliente Luce:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-secondary text-dark">
                        <td>{{$Utenze->contatoregas}}</td>
                        <td>{{$Utenze->codiceclientegas}}</td>
                        <td>{{$Utenze->contatoreacqua}}</td>
                        <td>{{$Utenze->codiceclienteacqua}}</td>
                        <td>{{$Utenze->contatoreluce}}</td>
                        <td>{{$Utenze->codiceclienteluce}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-12">
        @if ($Proprieta->inquilino_id != null)
            <h1>{{$Inquilino->nome}}</h1>
            <table class="table table-dark text-center table-responsive-lg table-hover table-bordered">
                <thead>
                    <tr class="text-uppercase">
                        <th>Nome:</th>
                        <th>Cognome:</th>
                        <th>Eta:</th>
                        <th>Codice Fiscale:</th>
                        <th>Documento:</th>
                        <th>Tel/Cel:</th>
                        <th>Email:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-secondary text-dark">
                        <td>{{$Inquilino->nome}}</td>
                        <td>{{$Inquilino->cognome}}</td>
                        <td>{{$Inquilino->eta}}</td>
                        <td>{{$Inquilino->cf}}</td>
                        @if(strlen($Inquilino->documento) < 31) <td class="text-danger">Nessun Documento</td>
                        @else <td><a href="{{$Inquilino->documento}}">Visualizza Documento</a></td>
                        @endif
                        <td>{{$Inquilino->tel}}</td>
                        <td>{{$Inquilino->email}}</td>
                    </tr>
                </tbody>
            </table>
        @else
            <h3>Questo immobile non ha un inquilino!</h3>
        @endif

    </div>
  </div>
</div>
@endsection