@extends('components.layout')
@section('content')
<x-widget/>
<div class="container my-5 py-5">
  <div class="row">
    <div class="col-12">
        <h1>{{$Inquilino->nome}} {{$Inquilino->cognome}}</h1>
        <table class="table table-dark text-center table-responsive-lg table-hover table-bordered">
            <thead> 
                <tr class="text-uppercase"> 
                    <th>nome:</th>
                    <th>cognome</th>
                    <th>eta</th>
                    <th>codice fiscale</th>
                    <th>documento</th>
                    <th>tel/cell</th>
                    <th>email</th>
                </tr>
            </thead>
            <tbody>
                <tr class="table-secondary text-dark">
                    <td>{{$Inquilino->nome}}</td>
                    <td>{{$Inquilino->cognome}}</td>
                    <td>{{$Inquilino->eta}}</td>
                    <td>{{$Inquilino->cf}}</td>
                    @if(strlen($Inquilino->documento) < 31) <td class="text-danger">Nessun Documento</td>
                    @else <td><a href="{{$Inquilino->documento}}">Visualizza Documento</a></td>
                    @endif
                    <td>{{$Inquilino->tel}}</td>
                    <td>{{$Inquilino->email}}</td>
                </tr>
            </tbody>
        </table>
        <ul>

        </ul>          
    </div>
  </div>
</div>
@endsection