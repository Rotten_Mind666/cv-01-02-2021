@extends('components.layout')

@section('content')
<x-widget/>


    <table class="table table-dark text-center table-responsive-lg">
        <thead>
            <tr> 
                <th scope="col">ID</th>
                <th scope="col">NOME</th>
                <th scope="col">COGNOME</th>
                <th scope="col">ETA</th>
                <th scope="col">CODICE FISCALE</th>
                <th scope="col">DOCUMENTO</th>
                <th scope="col">TEL</th>
                <th scope="col">EMAIL</th>
                {{-- <th id="edit" scope="col">MODIFICA</th> --}}
                <th id="delete" scope="col">ELIMINA</th>
                <th id="show" scope="col">VEDI DETTAGLI</th>
            </tr>
        </thead>
        <tbody>
                @foreach ($Inquilino as $item)
                    <tr class="text-capitalize">
                        <td>{{$item->id}}</td>
                        <td>{{$item->nome}}</td>
                        <td>{{$item->cognome}}</td>
                        <td>{{$item->eta}}</td>
                        <td>{{$item->cf}}</td>

                        @if(strlen($item->documento) < 31) <td class="text-danger">Nessun Documento</td>
                        @else <td><a href="{{$item->documento}}">Visualizza Documento</a></td>
                        @endif

                        <td>{{$item->tel}}</td>
                        <td>{{$item->email}}</td>
                        {{-- <td><a href="{{route('Proprieta.editInquilino', $item)}}" class="btn btn-outline-warning" id="edit">MODIFICA</a></td> --}}
                        <td><form action="{{route('Proprieta.destroyInquilino', $item)}}" method="post">@csrf @method('DELETE') <button type="submit" class="btn btn-outline-danger">ELIMINA</button></form></a></td>
                        <td><a href="{{route('Proprieta.showInquilino', $item)}}" class="btn btn-outline-white" id="show">VEDI TUTTO</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
@endsection