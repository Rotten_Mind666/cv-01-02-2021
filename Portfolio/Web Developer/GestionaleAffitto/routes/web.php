<?php

use App\Models\Inquilino;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InquilinoController;
use App\Http\Controllers\ProprietaController;

// INDEX
Route::get('/',[ProprietaController::class, 'index'])->name('welcome');

// CREATE INQUILINO
Route::get('/crea',[ProprietaController::class, 'create'])->name('proprieta.create');

// CREATE IMMOBILE
Route::get('/creaImmobile',[ProprietaController::class, 'createImmobile'])->name('proprieta.createImmobile');

// STORE
Route::post('/store',[ProprietaController::class, 'store'])->name('store');

// SHOW IMMOBILE
Route::get('/show/proprieta/{Proprieta}',[ProprietaController::class, 'showImmobile'])->name('Proprieta.showImmobile');
Route::get('/show/inquilino/{Inquilino}',[ProprietaController::class, 'showInquilino'])->name('Proprieta.showInquilino');
// SHOW INQUILINO

// TUTTO
Route::get('/tutto/Inquilini',[ProprietaController::class, 'tutto'])->name('Proprieta.tutto');
Route::get('/tutto/Immobili',[ProprietaController::class, 'tuttoImmobili'])->name('Proprieta.tuttoImmobili');
// TUTTO IMMOBILE

// EDIT INQUILINO
Route::get('/Modifica/{Inquilino}/inquilino',[ProprietaController::class, 'editInquilino'])->name('Proprieta.editInquilino');
Route::get('/edit/{Proprieta}/proprieta',[ProprietaController::class, 'editImmobile'])->name('Proprieta.editImmobile');
// EDIT PROPRIETA

// DELETE INQUILINO
route::delete('/Proprieta/{Proprieta}/delete', [ProprietaController::class, 'destroyProprieta'])->name('Proprieta.destroyProprieta');
route::delete('/Inquilino/{Inquilino}/delete', [InquilinoController::class, 'destroyInquilino'])->name('Proprieta.destroyInquilino');
// DELETE PROPRIETA

// // UPDATE
// Route::post('/Immobile/Update/{Proprieta}', [ProprietaController::class, 'update'])->name('update');
