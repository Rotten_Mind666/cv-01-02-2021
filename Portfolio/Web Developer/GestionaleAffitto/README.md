                <div class="form-group p-2">
                    <h2 class="font-weight-bold">Tipo Immobile</h2>
                    {{-- CATEGORIA --}}
                    <label for="descrizione">Categoria Catastale:</label>
                    <select class="form-control" type="text" name="descrizione" id="descrizione">
                        @foreach ($Categoria as $item)
                            <option value="{{$item->id}}">{{$item->anteprima()}}</option>
                        @endforeach
                    </select>
                    {{-- CATEGORIA --}}
                
                    {{-- TIPO --}}
                    <label for="tipo">Tipo di immobile:</label>
                    <select class="form-control" type="text" name="tipo" id="tipo">
                        @foreach ($Tipo as $item)
                            <option value="{{$item->id}}">{{$item->tipo}}</option>
                        @endforeach
                    </select>
                    {{-- TIPO --}}

                    {{-- UTILIZZO --}}
                    <label for="utilita">Utilizzo del immobile:</label>
                    <select class="form-control" type="text" name="utilita" id="utilita">
                        @foreach ($Utilizzo as $item2)
                            <option value="{{$item2->id}}">{{$item2->utilita}}</option>
                        @endforeach
                    </select>
                    {{-- UTILIZZO --}}
                </div>
                                    <select class="form-control" type="text" name="terrurb" id="terrurb">
                        <option value="Terreno">Terreno</option>
                        <option value="Urbano">Urbano</option>
                    </select>
                    <label for="intpor">Intero / Porzione</label>
                    <select class="form-control" type="text" name="intpor" id="intpor">
                        <option value="Intero">Intero</option>
                        <option value="Porzione">Porzione</option>
                    </select>